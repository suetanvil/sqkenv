#!/bin/bash

set -e

# Import utilities
SQKENV_ROOT="$(cd -P "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd)"
. "$SQKENV_ROOT/scripts/lib/common.sh"

parse_args "$@" <<EOF
Remove a Squeak version previously installed with 'install'.

  sqkenv uninstall <version>

EOF


function uninstall_version() {
    local version="$1"

    # Ensure it's been provided by the user.
    [ -n "$version" ] || die "No Squeak version given."

    # Ensure there's a spec for this
    spec=$(get_spec $version)
    [ -d "$spec" ] || \
        die "Version '$version' does not exist or was not installed by sqkenv."

    # Ensure it's present
    local instpath=$(versionpath "$version")
    [ -f "$instpath/version_info.sh" ] || \
        die "No version '$version' installed."


    # We cd to the root and do a very careful rm -rf with no wildcards
    # or absolute paths so that unexpected variable values are less
    # likely to have collateral damage.
    local pwd=`pwd`
    cd "$(dirname "$instpath")"
    local ipbase="./$(basename "$instpath")"
    [ -d "$ipbase" ] || die "INTERNAL ERROR: can't find '$ipbase'"
    rm -rf "$ipbase"
    cd "$pwd"

    echo "Uninstalled $version"
}

uninstall_version "$ARG_1"
