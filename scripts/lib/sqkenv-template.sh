#!/bin/bash

set -e

# Import utilities
SQKENV_ROOT="$(cd -P "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd)"
. "$SQKENV_ROOT/scripts/lib/common.sh"

parse_args "$@" <<EOF
Create a project template from the given image.

  sqkenv template [options] <image-name>

A template is just an image and changeset pair that can be installed
by 'sqkenv init' instead of the default pair.  This is handy if (e.g.)
you have a bunch of extensions and customizations you use regularly.

This command copies the given image (and corresponding changes) into
the set of templates sqkenv maintains for this Squeak version.

Options:

  --name NAME       - Specify the new template's name.  Defaults to 
                      the image name (minus the .image suffix).

  --overwrite       - If there is already a template with this name,
                      replace it with the current one.

EOF

# Retrieve this project's Squeak version and import its corresponding
# definitions.
have_proj || die "Not a sqkenv project."
. $SQKENV_PROJFILE
. $SQKENV_ROOT/versions/$PROJ_VERSION/version_info.sh

function pick_image() {
    local image="$1"
    image=${image%.image}

    [ -n "$image" ] || die "No image name provided."

    for f in $image.image $image.changes; do
        [ -f "$f" ] || die "Can't find '$f'"
    done

    echo $image
}

function pick_name() {
    local name="$1"
    local image="$2"

    [ -n "$name" ] || name=$image
    echo $name
}

function make_template() {
    local name="$1"
    local image="$2"
    local overwrite="$3"

    local path=$(templatespath "$PROJ_VERSION")

    [ -d "$path" ] || mkdir -p "$path"

    local backupdir=""
    if [ -d "$path/$name" ]; then
        [ -n "$overwrite" ] || die "Already a template named '$name'"

        # If --overwrite was given, we first move the old version to a
        # backup so the use can recover if something fails.
        backupdir="$path/$name.$(date +'%s.%N')"
        mv "$path/$name" $backupdir
    fi

    echo "Creating template '$name' from image '$image.image'"

    mkdir "$path/$name"
    cp "$image.image" "$path/$name/sqk.image"
    cp "$image.changes" "$path/$name/sqk.changes"

    echo "Compressing..."
    gzip -9n "$path/$name"/*

    if [ -n "$backupdir" -a -d "$backupdir" ]; then
        local pwd=`pwd`
        cd "$backupdir"
        rm *.gz
        cd "$pwd"
        rmdir "$backupdir"
    fi
    
    echo "Created template '$name' in '$path/$name/'"
}


image=$(pick_image "$ARG_1")
name=$(pick_name "$ARG_name" "$image")

make_template "$name" "$image" "$FLAG_overwrite"
