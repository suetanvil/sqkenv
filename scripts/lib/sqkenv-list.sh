#!/bin/bash

set -e

# Import utilities
SQKENV_ROOT="$(cd -P "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd)"
. "$SQKENV_ROOT/scripts/lib/common.sh"

#summary "List available versions and images"

parse_args "$@" <<EOF
List available versions and templates.

  sqkenv list

Shows each image and corresponding project templates.
EOF


first=yup
for ver in $(get_installed_versions); do
    [ -z "$first" ] && echo
    first=
    
    echo $ver

    for template in $(get_templates_for_version $ver); do
        echo "  $template"
    done
done
