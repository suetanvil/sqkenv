#!/usr/bin/env perl


=pod

A simple but complete argument parser for bash scripts.

It takes a long description of the program on stdin and the script's
"$@" as its argument list, then writes out a bash script that must
be evaluated.

WARNING: DO NOT use this if you can't trust the input.  A sufficiently
smart/motivated user can probably trick this into running other
commands.  If that's a problem, DON'T USE THIS.

Options '--help' and '--help-short' print (some of) the description as
a help message and exits with status 0.

Errors will print an error message and exit with status 1.

Valid arguments define (global, non-exported!) shell variables:

- FLAG_<name>=1 for each argument given.

- ARG_<name>=<option argument> for the corresponding option argument
  if required.

- ARG_<num>=<position-argument> for each positional argument (starting at 1).

Note that this uses the bash "$'...'" notation.

Example:

    eval $(perl parse_args.pl "$@" <<EOF
    Create life on a new planet

    This requires you to be in god mode.

    Options:

        --intelligent       - create (allegedly) intelligent life
        --type LIFETYPE     - one of "organic", "machine" or "energy"
        --dry-run           - do not create actual life

    Note: using this program is probably a bad idea.

    EOF
    )

=cut


use strict;
use warnings;

use Getopt::Long;


{
  my ($summary, $text, $options) = parse_option_desc();
  my $opt_vals = parse_cmdline($options);

  if ( !defined($opt_vals) ) {
    print_msg("Invalid arguments; try --help", 0);
  }
  elsif ( optval($opt_vals, "help")) {
    print_msg($text, 1);
  }
  elsif ( optval($opt_vals, "help-short") ) {
    print_msg($summary, 1);
  }
  else {
    print_arg_settings($options, $opt_vals);
  }
};


sub sanitize_string {
  my ($str) = @_;

  # Escape backslashes
  $str =~ s{\\}{\\\\}g;

  # Escape single quotes
  $str =~ s{'}{\\'}g;

  return $str;
}


sub print_arg_settings {
  my ($optnames, $opt_vals) = @_;

  for my $nm (@{$optnames}) {
    my $has_arg = ($nm =~ /=s$/);
    $nm =~ s/=s//;

    my $shellvar = $nm;
    $shellvar =~ s/-/_/g;

    next unless optval($opt_vals, $nm);

    if ($has_arg) {
      print "ARG_$shellvar=\$'", sanitize_string($opt_vals->{$nm}), "'; \n";
    }

    # FLAG_* is always set if the arg is given.
    print "FLAG_$shellvar=1; \n";
  }

  # Now, we do the positional arguments.
  my $count = 1;
  for my $arg (@ARGV) {
    print "ARG_$count=\$'", sanitize_string($arg), "'; \n";
    ++$count;
  }
}


# Return the option or "" if it is not present in the hash
sub optval {
  my ($opt_vals, $key) = @_;

  return "" unless exists($opt_vals->{$key});
  return $opt_vals->{$key};
}


# Emit shell code to print a message and exit.  Message may be a
# single string or a list of strings and should be chomped.  If
# $success is false, exit with a non-zero status.
sub print_msg{
  my ($msg, $success) = @_;
  $msg = [$msg] unless ref($msg);

  for my $line (@{$msg}) {
    $line = sanitize_string($line);
    print "echo \$'" . $line . "'; \n";
  }

  print "exit @{[$success ? '0' : '1']}; \n"
}


sub parse_cmdline {
  my ($option_names) = @_;

  push @{$option_names}, "help", "help-short";

  my $opts = {};
  GetOptions($opts, @{$option_names}) or return undef;

  return $opts;
}


sub parse_option_desc {
  my @text = ();
  my @options = ();

  while(<STDIN>) {
    chomp;
    s/\s*$//;

    # The entire description is the help text so we keep all of it.
    push @text, $_;

    # If it's an option, parse it.
    /^\s*--([a-z0-9][-a-z0-9]*) \s+ ([A-Z_]*) \s* - \s* (.*)/x and do {
      my $flagtext = $1;
      my $metavar = $2;

      $flagtext .= "=s" if $metavar;
      push @options, $flagtext;
    };
  }

  # Strip leading and trailing empty lines
  shift @text until $text[0];
  pop @text until $text[-1];

  # Then add exactly one of each:
  push @text, "";
  unshift @text, "";

  my $summary = $text[1];

  return ($summary, \@text, \@options);
}
