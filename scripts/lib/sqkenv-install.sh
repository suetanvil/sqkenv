#!/bin/bash

set -e

# Import utilities
SQKENV_ROOT="$(cd -P "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd)"
. "$SQKENV_ROOT/scripts/lib/common.sh"

parse_args "$@" <<EOF
Install a new Squeak version from the list of available ones.

  sqkenv install <squeak-version>
  sqkenv install --list

The requested Squeak binaries are installed using git and 
made available for starting new projects.

Options:

  --list            - Print a list of available Squeak versions, 
                      then exit.

EOF

function install_version() {
    local version="$1"
    [ -n "$version" ] || die "No version specified."

    local instpath=$(versionpath "$version")
    [ -d "$instpath" ] && \
        die "Version '$version' is already installed."

    local spec=$(get_spec $version)
    [ -d "$spec" ] || die "Unknown version '$version'"

    . "$spec/install_info.sh"

    # Ensure the versions directory exists.
    [ -d "$SQKENV_ROOT/versions" ] || mkdir "$SQKENV_ROOT/versions"

    local dest_dir="$SQKENV_ROOT/versions/$version"
    git_clone_version_files $dest_dir $VERS_BRANCH_NAME $VERS_COMMIT_ID

    cp $spec/version_info.sh $instpath/
}
    
function get_available_versions() {
    for ver in $(get_supported_specs); do
        if [ -d $(versionpath $ver) ]; then
            ver="$ver (installed)"
        fi

        echo $ver
    done
}


if [ -n "$FLAG_list" ]; then
    get_available_versions
    exit 0
else
    install_version "$ARG_1"
fi

