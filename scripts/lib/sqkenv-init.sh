#!/bin/bash

set -e

# Import utilities
SQKENV_ROOT="$(cd -P "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd)"
. "$SQKENV_ROOT/scripts/lib/common.sh"

parse_args "$@" <<EOF
Start a new project in the current directory

  sqkenv init [options] <squeak-version>

Copies or symlinks all files needed to start using the specified
version of Squeak and creates a project file used by sqkenv to
identify the corresponding VM.

Options:

  --name PROJNAME   - Name of the initial image to create in this
                      project directory.

  --template ID     - Use the given template (i.e. starter image) from
                      which to start this project instead of the default.

EOF


# Ensure there isn't already a project here
have_proj && die "Project already defined."

# Get the Squeak version and import the relevant details
version="$ARG_1"
[ -n "$version" ] || \
    die "No version given; see 'sqkenv init --help' for details"

# Retrieve the details of this version (or die trying).
[ -d "$SQKENV_ROOT/versions/$version" ] || die "No version '$version'"
. "$SQKENV_ROOT/versions/$version/version_info.sh"

# Find the template if requested and ensure it exists; otherwise,
# select the default image.
if [ -n "$ARG_template" ]; then
    imagedir=$(templatespath $version)/"$ARG_template"
    [ -f "$imagedir/sqk.image.gz" ] || \
        die "Can't find template image '$ARG_template'"
    default_name="$ARG_template"
    echo "Using template '$ARG_template'"
else
    imagedir="$SQKENV_ROOT/versions/$version/base_img"
    default_name="$version"
    echo "Using default image for $version"
fi

# Select the initial image name or use the directory name as a
# default.
name="$ARG_name"
[ -n "$name" ] || name=$default_name

# Create the sources symlink
ln -s "$SQKENV_ROOT/versions/$version/$VERS_SOURCEFILE" .

# Copy the files
for suffix in image changes; do
    img="${name}.$suffix"

    [ -e "$img.gz" -o -e "$img" ] && \
        die "Would overwrite '$img' or '$img.gz'"

    echo "Copying and decompressing '$img'"
    cp "$imagedir/sqk.${suffix}.gz" "$img.gz"
    gunzip $img.gz
done

# Create the projfile
(
    echo "# sqkenv project configuration"
    echo
    echo "# Squeak version of this project"
    echo "PROJ_VERSION=$version"
    echo
    echo "# VM options (if needed)"
    echo "# VERS_VM_OPTS="
)> $SQKENV_PROJFILE

echo "Done."

