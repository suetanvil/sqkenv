#!/bin/bash

set -e

# Import utilities
SQKENV_ROOT="$(cd -P "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd)"
. "$SQKENV_ROOT/scripts/lib/common.sh"

parse_args "$@" <<EOF
Start the vm on the current project.

  sqkenv run [options] [image-name]

If there is exactly one image file present in the current directory,
it will be used by default if the image name is omitted.  Otherwise,
it is an error to not provide an image name.

You can add extra VM options by setting the 'VERS_VM_OPTS' variable in
$SQKENV_PROJFILE.

Options:

  --foreground      - Run the command in the foreground.  By default,
                      it is run as a background process under nohup.

  --print           - Print the command that would be executed and quit.

EOF


# Retrieve this project's Squeak version and import its corresponding
# definitions.
[ -f $SQKENV_PROJFILE ] || die "Not a sqkenv project."
. $SQKENV_PROJFILE
. $SQKENV_ROOT/versions/$PROJ_VERSION/version_info.sh


if [ -n "$ARG_1" ]; then
    image="${ARG_1%.image}.image"
    [ -f "$image" ] || die "Can't find image file '$image'"

else
    image=`ls -1 *.image | head -1`
    [ -n "$image" ] || die "Can't find an image to load."
    [ "$(ls -1 *.image | wc -l)" = "1" ] || \
        die "Multiple image files present; specify the one you want on the command line"
fi


cmdline="$(vmpath) $VERS_VM_OPTS $image"

if [ -n "$FLAG_print" ]; then
    echo $cmdline
    exit 0
fi

if [ -z "$FLAG_foreground" ]; then
    cmdline="nohup $cmdline &"
fi

eval "exec $cmdline"

