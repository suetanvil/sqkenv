#!/bin/bash

# Common utilities and globals

# Absolute path to the sqkenv installation.  This is likely already
# defined, but we'll do it again just in case.
SQKENV_ROOT="$(cd -P "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd)"

# Name of the setup file
SQKENV_PROJFILE="sqkenv-version"

# Load settable constants; we let the user override them if needed.
. "$SQKENV_ROOT/scripts/lib/default_config.sh"
[ -f "$SQKENV_ROOT/config.sh" ] && . "$SQKENV_ROOT/config.sh"



# Print an error message to stderr and exit with status 1
function die() {
    echo >&2 $*
    exit 1
}

# Ensure that required programs are present and in the path.
function check_system() {

    # It's unlikly we can get this far without bash, but who knows?
    [ -n "$BASH_VERSION" ] || die "sqkenv needs to be run with 'bash'."

    # It's more likely that we're using an older bash; it's possible
    # that it will even work, but I don't have the means to test it so
    # I'm limiting use to 5.0 or later.
    if (( ${BASH_VERSINFO[0]} < 5 )) ; then
        die "sqkenv needs bash 5.0 or later to work."
    fi

    # Check for dependencies.
    for tool in git perl gunzip ; do
        which "$tool" > /dev/null 2>&1 || \
            die "Can't find '$tool'."
    done
}


# Test if the given executable is in the path
function has() {
    if `type -t "$1" >/dev/null 2>&1 ` ; then
        return 0
    fi

    return 1
}

function parse_args() {
    local script=$(perl $SQKENV_ROOT/scripts/lib/parse_args.pl "$@" || \
                       echo "exit 2")
    eval "$script"
}

# Return the absolute path to the root of the requested version
function versionpath() {
    local version=$1
    [ -n "$version" ] || version=$PROJ_VERSION
    echo "$SQKENV_ROOT/versions/$version"
}

function vmpath() {
    local version=$1
    [ -n "$version" ] || version=$PROJ_VERSION
    echo "$(versionpath $version)/$VERS_VM"
}



function have_proj() {
    [ -f $SQKENV_PROJFILE ]
}


# Copy $1 to $2 unless $2 already exists, in which case fail with an
# error.  If $2 is a directory, then there must not already be a file
# with that name there.
function inst() {
    local src="$1"
    local dest="$2"

    [ -f "$src" ] || die "Source must be a file."

    if [ -d "$dest" ]; then
        dest="$dest"/$(basename "$1")
    fi

    [ -e "$dest" ] && die "File '$dest' already exists."

    cp "$src" "$dest"
}



function get_installed_versions() {
    for i in $SQKENV_ROOT/versions/*; do
        if [ -d "$i" ] && [ -f "$i/version_info.sh" ]; then
            basename "$i"
        fi
    done
}    


# Return the absolute path to the root of the requested version
function templatespath() {
    local version=$1
    #[ -x "$version" ] || version=$PROJ_VERSION
    echo "$SQKENV_ROOT/templates/$version"
}


function get_templates_for_version() {
    local sp=$(templatespath $1)

    [ -d "$sp" ] || return 0

    for template in "$sp"/*; do
        [ -d "$template" ] && basename $template
    done
}


function platform_desc() {
    local desc="`arch`-`uname -s | tr A-Z a-z`"

    # Squeak/OpenSmalltalk seems to be pretty cross-distro on Linux so
    # I'm hoping we can get away with just treating Linux as a
    # monolith.  If not, this will need to be extended to include
    # special cases with generic "linux" as a fallback.  But that's
    # for another day.
    
    echo $desc
}


function get_spec() {
    local version="$1"

    local spec="$version-$(platform_desc)"
    echo "$SQKENV_ROOT/scripts/specs/$spec"
}

function get_supported_specs() {
    local platform=$(platform_desc)

    for ver_path in $SQKENV_ROOT/scripts/specs/*-$platform; do
        if [ ! -f "$ver_path/install_info.sh" ]; then
            continue
        fi
        basename $ver_path | sed -e 's/-.*//'
    done
}



function git_clone_version_files() {
    local dest_dir="$1" branch="$2" commit="$3"
    local cwd=`pwd`

    # We clone the whole thing into a temp directory first and then
    # move it to $dest_dir afterward.  This way, if there's a error
    # the checkout stays out of sqkenv.
    #
    # (There are also security implications; if the asset repo was
    # tampered with in a way that changes content, it will have also
    # invalided the commit ID in which case the 'git checkout' will
    # fail; we *don't* want a potentially runnable executable in the
    # 'versions' directory afterward.(
    
    local tmpdir=$(mktemp -d /tmp/sqkenv.checkout.XXXXXXXXXXXX)

    # First, we clone just this branch; this should get us only
    # the files we need.
    echo "Retrieving version from git repo '$ASSET_REPO'"
    if ! git clone --branch "$branch" $ASSET_REPO $tmpdir ; then
        rm -rf $tmpdir
        die "Error cloning $ASSET_REPO"
    fi

    cd $tmpdir

    # Now, checkout the specified commit.
    echo "Checking out the specified commit..."
    if ! git checkout -q "$commit"; then
        cd $pwd
        rm -rf $tmpdir
        die "Error checking out specified commit"
    fi

    # We're never going to use this as a git repo so we may as well
    # delete this to free up (a lot of) space
    rm -rf .git

    # And move the directory
    cd $cwd
    mv "$tmpdir" "$dest_dir"

    echo "Done."
}
