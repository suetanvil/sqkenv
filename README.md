# sqkenv - a tool to manage Squeak versions and projects

`sqkenv` is a simple command-line tool for using Squeak Smalltalk (and
related languages) in a typical Unix environment.  It is inspired by
(but shares no code with) [rbenv](https://github.com/rbenv/rbenv).


## Overview

`sqkenv` does two things for you:

1. It painlessly installs one or more (supported) versions of Squeak
   or a Squeak-derived language.

2. It creates and manages "project" directories for your work. Each
   project is initialized with a starter image and sources files and
   `sqkenv run` will execute the correct VM for it.


## Installation

Simply clone this repository somewhere and then add `scripts/bin` to
your PATH:

    git clone <path-to-this-repo> ~/.sqkenv
    export PATH=~/.sqkenv/scripts/bin:$PATH

Your system will need to have `git` installed, of course.  `sqkenv`
uses a number of shell utilities (and `perl`) but all of these are
also needed by `git` so if that works, you're probably good.

You can update by doing a `git pull`, assuming updates exist.


## Quick Walkthrough

Once you've installed `sqkenv`, you will want to install one or more
Squeak versions.  You can get a list of available versions with:

    sqkenv install --list

Once you've chosen a version (e.g. Squeak 6.0), install it with
`sqkenv install`:

    $ sqkenv install squeak6.0

Now, you can create your initial project, 'my_project'.  `sqkenv`
expects each project to be in its own directory.  The name is not
important but we use 'my_project' for consistency:

    $ mkdir my_project
    $ cd my_project

We then intialize the project:

    $ sqkenv init squeak6.0 --name my_project

This will create the following files:

    my_project.changes
    my_project.image
    sqkenv-version
    SqueakV60.sources

These are the usual three Squeak files (the .sources file is a
symlink) and `sqkenv-version`, a config file used to identify the
version.  It can also be edited to add command-line arguments to the
VM.

You can launch squeak like this:

    $ sqkenv run

Since Squeak will let you save your image under another name, you may
have multiple images in your project directory.  `sqkenv run` will let
you select them:

    $ sqkenv run my_project_dangerous_experiment

The image name is required unless there is only one image present.

By default, `sqkenv run` will use `nohup`; expect to find `nohup.out`
files appearing in the project directory.  You can force it into the
foreground with the `--foreground` flag or to print the command
without running it with `--print`.

You can also turn an image (plus sources) into a **template**.  This
is an image/sources pair that can be used to start a new project.
This is handy if you have a set of customizations you normally import
into your image and want to skip the time and hassle of recreating
them for each new project.

Simply start a project, edit the image to where you want it to be and
then save it and exit. You can then turn it into a template with
`sqkenv template`:

    $ sqkenv template --name custom my_project.image 

Now, you can use 'custom' as the starting point for a new project:

    $ cd ..
    $ mkdir my_project_2
    $ cd my_project_2/
    $ sqkenv init squeak6.0 custom

Note that templates are tied to specific Squeak versions.




## Caveats

1. Currently, we only support two Squeak versions and one Pharos
   release, and then only on Linux.  Sorry about that.  I'm open to
   contributions.

2. Binaries are retrieved using `git` and tied to specific commit IDs,
   so they can't be tampered with underway. However, you still need to
   trust me to not have tampered with it. (If you don't, you can
   checkout the commit, audit `remake.sh` and then rerun it and see if
   any of the files have changed, but that's a lot of effort.)


## Security

Like anything that downloads programs from the Internet and then runs
them, there is a certain amount of risk associated with this tool.
That being said, I think I've done a reasonable job of preventing bad
actors from exploiting this:

1. The binaries are retrieved from
   [this git repository](https://codeberg.org/suetanvil/sqkenv-assets). They
   are identified by a specific commit-id (i.e. a secure hash) in your
   `sqkenv` checkout so you are guaranteed to get exactly the files I
   put in.  The only way a bad buy could change that is by editing
   your particular `sqkenv` checkout and if that happens, you have
   worse problems.

2. Each of the binary collections comes with a script that fetches
   the files from the original sources, unpacks them and selects the
   stuff we need.  If you check out a particular asset set and rerun
   this script, you can confirm that nothing has changed with a `git
   status`.  (Of course, this depends on the upstream not changing
   things; I have no control of that.)

I'm sure someone cleverer than I can come up with a way to exploit
this, but there's a lot of lower-hanging fruit than this little
project.



